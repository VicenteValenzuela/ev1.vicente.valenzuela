/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model;

/**
 *
 * @author valen
 */
public class CalcularInteres {

    private double capital, interes_anual, anios;
    private double Cal;
    
    
    
    /**
     * @return the anios
     */
    public double getAnios() {
        return anios;
    }

    /**
     * @param anios the anios to set
     */
    public void setAnios(double anios) {
        this.anios = anios;
    }

    /**
     * @return the Cal
     */
    public double getCal() {
        return Cal;
    }

    /**
     * @param Cal the Cal to set
     */
    public void setCal(double Cal) {
        this.Cal = Cal;
    }
    
     
    

    /**
     * @return the capital
     */
    public double getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(double capital) {
        this.capital = capital;
    }

    /**
     * @return the interes_anual
     */
    public double getInteres_anual() {
        return interes_anual;
    }

    /**
     * @param interes_anual the interes_anual to set
     */
    public void setInteres_anual(double interes_anual) {
        this.interes_anual = interes_anual;
    }

    /**
     * @param capital
     * @return the años
     */
   
    
   
    public double getInteres(double capital, double interes_anual, double anios ){
        //double Cal=this.capital*(interes_anual/100)*anios;
        
        System.out.println("Entrando a funcion get interes");
        
        this.setCapital(capital);
        this.setInteres_anual(interes_anual);
        this.setAnios(anios);
        return getInteres();
    }
    
    
   public double getInteres (){
       this.Cal =this.capital*(this.interes_anual/100)*this.anios;
        return Cal;
   }
    
}
